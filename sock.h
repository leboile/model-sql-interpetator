#ifndef __linSock
#define __linSock
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <iostream>
using namespace std;
class linSock {
private:
    int sock;
    bool isServer;
    sockaddr_in sockaddress;
    struct hostent *phe;
    int fd[512];
    int fdInd;
    int len;
    fd_set server;
    int state;//-1 -error 0-initialized 1-connected 2- waiting for connection 3-not binded
public:
    linSock(const char* host,const unsigned short port)
    {
        sock=socket(PF_INET,SOCK_STREAM,0);
        sockaddress.sin_family=AF_INET;
        sockaddress.sin_port=htons(port);
        sockaddress.sin_addr.s_addr=INADDR_ANY;
        state=0;
        int s=1;
        setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&s,sizeof(s));
        if (bind(sock,(sockaddr*)(&sockaddress),sizeof(sockaddress)))
        {
            perror("Socket creation err: ");
            state=-1;
        }
        fdInd=0;
        state=0;
    }
    linSock()
    {
        state=3;
        sock=socket(PF_INET,SOCK_STREAM,0);
        fdInd=0;
    }
    int sockConnect(const char* host,const unsigned short port)
    {
        if (!(phe=gethostbyname(host)))
            perror("Host error: ");
        sockaddress.sin_family=AF_INET;
        sockaddress.sin_port=htons(port);
        memcpy(&sockaddress.sin_addr, phe->h_addr_list[0], sizeof(sockaddress.sin_addr));
        state=1;
        if (connect(sock,(sockaddr*)(&sockaddress),sizeof(sockaddress)))
        {
            perror("Connection err: ");
            state=-1;
            return -1;
        }
        *fd=sock;
        fdInd++;
        return 0;
    }
    virtual int send(const char* buf, const int len,int ind)
    {
        if (state!=1 && ind>=fdInd)
            return -2;
        FD_ZERO(&server);
        FD_SET(fd[ind],&server);
        if (select(FD_SETSIZE,NULL,&server,NULL,NULL) <=0)
        {
            perror("SELECT ERR SEND: ");
            state=-1;
        }
        if(write(fd[ind],buf,len)!=len) return 1;
        return 0;
    }
    int getConnection(int timeout)
    {
        if (state!=0 &&state!=2 && state!=1)
            return -1;
        if (state!=2 && listen(sock,5))
        {
            perror("Socket listening error: ");
            state=-1;
            return -1;
        }
        state=2;
        FD_ZERO(&server);
        FD_SET(sock,&server);
        struct timeval t;
        t.tv_sec=0;
        t.tv_usec=timeout;
        int p = select(FD_SETSIZE,&server,NULL,NULL,&t);
        if (p<0)
        {
            perror("SELECT ERR CON: ");
            state=-1;
            return -1;
        }
        else if (p!=0)
        {
            len=sizeof(sockaddress);
            if ((fd[fdInd++]=accept(sock,(sockaddr*)(&sockaddress),(socklen_t*)&len))==-1)
            {
                fdInd--;
                perror("Socket accept error: ");
                state=-1;
                return -1;
            }
            state=1;
            return 0;
        }
        return -1;
    }
    int receive(char* buf,const int maxlen,int ind,long timeout)
    {
        if (state!=1 && ind>=fdInd)
        {
            cout<<"OHNOES";
            return -1;
        }
        struct timeval t;
        t.tv_sec=timeout;
        t.tv_usec=0;
        FD_ZERO(&server);
        FD_SET(fd[ind],&server);
        int p;
        if (timeout==0)
            p=select(FD_SETSIZE,&server,NULL,NULL,NULL);
        else p=select(FD_SETSIZE,&server,NULL,NULL,&t);
        // cout<<"SELECT PASSED WITH "<<p<<endl;
        if (p==0) return -1;//TIME OUT
        int k=1;
        for(int i=0; i<maxlen; i++)
        {
            k=read(fd[ind],&(buf[i]),sizeof(char));
            if (k!=sizeof(char) || buf[i]=='\0' || buf[i]=='\n')
                break;
        }
        if (k<=0 || p<0)
        {
            dropConnection(ind);
            return 2;
        }
        //cout<<"fd[ind] "<<fd[ind]<<" ind "<<ind<<" buf: "<<buf<<" !"<<endl;
        // cout<<"PREPARING TO READ"<<endl;
        //cout<<"READ PASSED"<<endl;
        return 0;
    }
    int dropConnection(int ind)
    {
        close(fd[ind]);
        for(int i=ind; i<fdInd; i++)
            fd[i]=fd[i+1];
        fdInd--;
        return ind;
    }
    virtual ~linSock()
    {
        for(int i=0; i<fdInd; i++)
            close(fd[i]);
        close(sock);
    }
    int getState()
    {
        return state;
    }
    int getNumOfClients()
    {
        return fdInd;
    }
};
#endif
