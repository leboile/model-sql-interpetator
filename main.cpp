#include <iostream>
#include "parser.cpp"
#include "sock.h"
#define BUF_SIZE 1024
#define CLRBUF  for(int j=0;j<BUF_SIZE;j++) s[j]='\0'
using namespace std;
int main()
{
    linSock server("127.0.0.1",8235);
    char s[BUF_SIZE];
    cout<<"Server started"<<endl;
    while(1)
    {
        if (!server.getConnection(10))
        {
            cout<<"CLIENT CONNECTED"<<endl;
        }
// cout<<"NOT HANGING"<<endl;
        CLRBUF;
        int i=0;
        while(i<server.getNumOfClients())
        {
            //cout<<"CYCLE ENTERED"<<endl;
            CLRBUF;
            int r=server.receive(s,BUF_SIZE,i,1);
            // cout<<"RECEIVED "<<s<<endl;
            if(r>0)
            {
                cout<<"CLIENT DISCONNECTED"<<endl;
                CLRBUF;
                continue;
            }
            else if (strlen(s)>0 && r!=-1)
            {
                try
                {
                    cout<<"COMMAND: "<<s<<endl;
                    Parser p(s,BUF_SIZE,&server,i);
                    p.SQL();
                    server.send("SUCCESS\n",8,i);
                }
                catch (const char* err)
                {
                    server.send("ERROR: ",8,i);
                    server.send(err,strlen(err)+1,i);
                    server.send("\n",2,i);
                    cout<<"ERROR: "<<err<<endl;
                }
            }
        }
        i++;
    }
    return 0;
}

