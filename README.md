Task description is available in russian: cmcmsu.no-ip.info/download/model.sql.interpreter.2005.pdf 

I tried to build server (main.cpp) under modern cygwin and gcc like that

$ g++ main.cpp -o a.exe

no diagnostics were produced by compiler, and resulting executable seem to be functional. 

$ ./a.exe

Server started


I've tried the following in separate cygwin window and actually it all works! SUCCESS and ERROR are the server responses on my correct and incorrect input SQL statements.

$ g++ client.cpp -o client.exe

$ ./client.exe

Enter ip and port

localhost 8235

Succesfully connected.

CREATE TABLE TestTable (LONG, TEXT)

ERROR: NONRESERVED WORD EXPECTED

CREATE TABLE TestTable (Col1 LONG, Col2 TEXT)

ERROR: LEXICAL ERROR

CREATE TABLE TestTable (Col1 LONG, Col2 TEXT(20))

SUCCESS

INSERT INTO TestTable 1 "Test"

ERROR: '(' EXPECTED

INSERT INTO TestTable (1, "Test")

ERROR: LEXICAL ERROR

INSERT INTO TestTable (1, 'Test')

SUCCESS

INSERT INTO TestTable (2, 'Test2')

SUCCESS

INSERT INTO TestTable (3, 'Test3')

SUCCESS

SELECT * FROM TestTable

ERROR: 'WHERE' EXPECTED

SELECT * FROM TestTable WHERE ALL

1 Test

2 Test2

3 Test3

SUCCESS

SELECT * FROM TestTable WHERE Col1=2

2 Test2

SUCCESS