#if !defined(_Table_h_)
#define _Table_h

#define MaxFieldNameLen 30 /* ������������ ����� �������� � �������� ���� */
#ifdef  __cplusplus
extern "C" {
#endif
    enum Errors /* ���������� ��������� ���� ������ ��� ������ � ��������� */
    {
        OK,             /* ������ ��� */
        CantCreateTable,
        CantOpenTable,
        FieldNotFound,
        BadHandle,
        BadArgs,
        CantMoveToPos,
        CantWriteData,
        CantReadData,
        CorruptedData,
        CantCreateHandle,
        ReadOnlyFile,
        BadFileName,
        CantDeleteTable,
        CorruptedFile,
        BadPos,
        BadFieldType,
        BadFieldLen,
        NoEditing,
        BadPosition
    };

    /* ��������� ��������� �� ������� */
    extern const char* ErrorText[];

    /* ������ �������, � ������� �������� ������������, ������ ��������������� ���������� ���� THandle, ���������� �� ��������� �������� �������. ��� �������� ������ � ��������� ���������� ���� ���������� ��� ������ �� ���������� �������.*/

    typedef struct Table * THandle;

    /* ��������� ��� ���������� ��������� ���� ������, �������� � �������� */

    enum FieldType
    {
        Text,     /* ������ ( �� ����� 256 ��������) */
        Long      /* ����� ������� �������� ����� */
    };

    /* ��� ��������� ������ �������� ������ ���� ������� */

    struct FieldDef
    {
    public:
        char name[MaxFieldNameLen]; /* ��� ������� ���� */
        enum FieldType type; /* ��� ���� */
        unsigned len; /* ����� ���� � ������ */
    };

    /* ��������� ��������� ���������� ��������� ������� � ������������ ��� �������� ����� �������. ���������� �� ���� ��������� �������� � ��������� ����� */

    struct TableStruct
    {
    public:
        unsigned numOfFields; /* ����� ����� � ������� */
        struct FieldDef *fieldsDef; /* ������������ ������,
  ������ ������� �������� - �������� ���� ������� */
        ~TableStruct()
        {
            delete[] fieldsDef;
        }
    };

    /******************************************************/
    /*  ���� ����������� ��������� ������� ��� ������ 	*/
    /*  � ���������. ����� ������� ��������� ������� 		*/
    /*  ������� �����������							*/

    enum Errors createTable(char *tableName,
                            struct TableStruct *tableStruct);

    /* ������� ������� ����� ������� � �������� ������ � ����������. ��� ���� ��������� ����� ���� � ������� ����������, � ��������� �������� ����� ����������� ��������� ������� */

    enum Errors deleteTable(char *tableName);

    /* ������� ������� ������� � �������� ������ */

    enum Errors openTable(char *tableName, THandle *tableHandle);

    /* ������� ��������� ������� � �������� ������ ��� ������. �� ������ tableHandle ������������ ����������, ������� ������ �������������� ��� ������ �� ������ �������. ��� ���� ����������� ���� �������. �� ������ ��������� ����� �������, ��������� ���������� � ��������� �������, � ����������� ������ ����������� ��������������� ������� ���� struct TableStruct. ��������� ������ ��� ������ � ������� ������� � ��� �������� ����� ������. ������ ������� ������������ �� ������ ��������� �������. */

    enum Errors closeTable(THandle tableHandle);

    /* ������� ��������� ������� ��� �������������. ��� ���� ������������� �����, ���������� ������ ��������,� ������, ����������� ���� �������. */

    enum Errors moveFirst(THandle tableHandle);

    /* ������� ������������� ��������� ����� �� ������ ������ (���� ��� ����) � ��������� ������ � ����� ������� ������. ���� ������� �����, �� ��������� ������ ������� ������ �� ����������. ��� ���� ������� afterLast � beforeFirst ������ �������� TRUE.*/

    enum Errors moveLast(THandle tableHandle);

    /* ������� ������������� ��������� ����� �� ��������� ������ (���� ��� ����) � ��������� ������ � ����� ������� ������.. ���� ������� �����, �� ��������� ������ ������� ������ �� ����������. ��� ���� ������� afterLast � beforeFirst ������ �������� TRUE.*/

    enum Errors moveNext(THandle tableHandle);

    /* ������� ������������� ��������� ����� �� ��������� � ����� ������ (���� ��� ����) � ��������� ������ � ����� ������� ������.. ���� ����� ��� ��������� �� ��������� ������, �� �� ��������� � ��������� "����� ���������", � ������� ���������� ������ �� ����������. ��� ���� ������� afterLast ������ �������� TRUE.*/


    enum Errors movePrevios(THandle tableHandle);

    /* ������� ������������� ��������� ����� �� ���������� ������ (���� ��� ����) � ��������� ������ � ����� ������� ������.. ���� ����� ��� ��������� �� ������ ������, �� �� ��������� � ��������� "����� ������", � ������� ���������� ������ �� ����������. ��� ���� ������� beforeFirst ������ �������� TRUE.*/

    bool beforeFirst(THandle tableHandle);

    /* ������� ������ �������� TRUE, ���� ������� ����� ��� ���� � ��������� "�� ������ ������" ����������� �������� movePrevios, ����� �������� �������� FALSE. */

    bool afterLast(THandle tableHandle);

    /* ������� ������ �������� TRUE, ���� ������� ����� ��� ���� � ��������� "�� ��������� ������" ����������� �������� moveNext, ����� �������� �������� FALSE. */

    enum Errors getText(THandle tableHandle, char *fieldName,char **pvalue);

    /* ������� ����������� ���������� pvalue ��������� �� ������ - �������� ���� fieldName. */

    enum Errors getLong(THandle tableHandle, char *fieldName,
                        long *pvalue);

    /* ������� ����������� ���������� pvalue ����� ����� - �������� ���� fieldName. */

    enum Errors startEdit(THandle tableHandle);

    /* ������� ������������ ����� ������� �������������� ������� ������ */

    enum Errors putText(THandle tableHandle, char *fieldName,
                        char *value);

    /* ������� ����������� ���� fieldName - ��������-������ */

    enum Errors putLong(THandle tableHandle, char *fieldName,
                        long value);

    /* ������� ����������� ���� fieldName �������� - ����� ����� */

    enum Errors finishEdit(THandle tableHandle);

    /* ������� ����������� �������������� ������� ������ (� ������). ��� ���� ������������ ������ ����������� ������ � ����. */

    enum Errors createNew(THandle tableHandle);

    /* ������� �������� ����� ����� ������, ��� ������������ ����� ����������� ������ ����� ������ ����������� ���������� */

    /* ��������� ��� ������� ����������� �������� ��������������� ����� � ������ ����� ������. */

    enum Errors putTextNew(THandle tableHandle, char*fieldName, char *value);
    enum Errors putLongNew(THandle tableHandle,
                           char * fieldName, long value);

    enum Errors insertNew(THandle tableHandle);

    /* ������� ��������� ����� ������ �� ������ ����� ������ ����� ������� ������� (�� ������� "������").*/

    enum Errors insertaNew(THandle tableHandle);

    /* ������� ��������� ����� ������ �� ������ ����� ������ � ������ �������.*/

    enum Errors insertzNew(THandle tableHandle);

    /* ������� ��������� ����� ������ �� ������ ����� ������ � ����� �������.*/

    enum Errors deleteRec(THandle tableHandle);

    /* ������� ������� ������� ������. ��� ����, ���� ���� ��������� ������, �� ��� ������������� ���������� �������, ���� ���, �� ���������� ��������� "����� ���������". */

    const char *getErrorString(enum Errors code);

    /* ������� �� ���� ������ ������ �� ������������� � ���� ������.*/

    enum Errors getFieldLen(THandle tableHandle,char*fieldName,unsigned *plen);

    /* ������� ������ ����� ���� ������� � ������. */

    enum Errors getFieldType(THandle tableHandle,
                             char *fieldName, enum FieldType *ptype);
    /* ������� ������ ��� ���� �������. */

    enum Errors getFieldsNum(THandle tableHandle,
                             unsigned *pNum);
    /* ������� ������ ����� ����� � �������. */

    enum Errors getFieldName(THandle tableHandle,
                             unsigned index, char **pFieldName);
    /* ������� �� ������ ���� � ������� ������ ��� ���. ���� ���������� � 1. */
#ifdef __cplusplus
}
#endif
#endif

