#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <iostream>
#include "sock.h"
#define CLRBUF  for(int j=0;j<BUF_SIZE;j++) buf[j]='\0'
#define BUF_SIZE 1024
void handler(int h)
{

}
using namespace std;
int main()
{
    linSock client;
    char ip[20];
    int port;
    char buf[BUF_SIZE]="";;
    cout<<"Enter ip and port"<<endl;
    cin>>ip>>port;
    if (client.sockConnect(ip,port))
    {
        cout<<"CONNECTION ERROR"<<endl;
        return 0;
    }
    CLRBUF;
    cout<<"Succesfully connected."<<endl;
    int a=fork();
    if (a<0)
        perror("FORK ERR: ");
    if (a==0)
        while(1)
        {
            CLRBUF;
            char c;
            for(int i=0; i<BUF_SIZE; i++)
            {
                c=cin.get();
                if (c=='\n' || i==BUF_SIZE-1)
                {
                    buf[i]='\0';
                    break;
                }
                buf[i]=c;
            }
            if (strlen(buf)>0)
            {
                //cout<<"THIS LINE: "<<buf<<endl;
                client.send(buf,strlen(buf)+1,0);
            }
        }
    while(1)
    {
        CLRBUF;
        if (client.receive(buf,BUF_SIZE,0,0)>0)
        {
            kill(a,SIGINT);
            wait(NULL);
            cout<<"CONNECTION DROPPED"<<endl;
            return 0;
        }
        else if(strlen(buf)>0)
        {
            cout<<buf;
        }
    }
    return 0;
}
