#include "sock.h"
#include "db.cpp"
#include <stdio.h>
#include <iostream>
using namespace std;
enum state {H,IDENT,DELIM,NUMB,STR};
const char * TW[]=
{
    "",
    "SELECT",
    "INSERT",
    "UPDATE",
    "DELETE",
    "CREATE",
    "BOOL",
    "DROP",
    "FROM",
    "WHERE",
    "INTO",
    "IN",
    "SET",
    "TABLE",
    "TEXT",
    "LONG",
    "LIKE",
    "NOT",
    "AND",
    "OR",
    "ALL",
    "*",
    NULL
};

const char* TD[]=
{
    "", // 0 ������� 0 �� ������������
    "\0", // 1
    ",", // 2
    "(", // 3
    ")", // 4
    "<", // 9
    ">", // 10
    "=",
    "!=", // 14
    "<=", // 15
    ">=",
    "+",
    "-",
    "%",
    "/",
    NULL
};
enum type_of_lex
{
    LEX_NULL, // 0
    LEX_SELECT,//1
    LEX_INSERT,//2
    LEX_UPDATE,//3
    LEX_DELETE,//4
    LEX_CREATE,//5
    LEX_DROP,//6
    LEX_FROM,//7
    LEX_WHERE,//8
    LEX_IN,//9
    LEX_INTO,//10
    LEX_SET,//11
    LEX_TABLE,//12
    LEX_LONG,//14
    LEX_TEXT,//13
    LEX_LIKE,//15
    LEX_NOT,//16
    LEX_AND,//17
    LEX_OR,//18
    LEX_ALL,//19
    LEX_COLON,//20
    LEX_COMMA,//21
    LEX_RBRACKET,//22
    LEX_BOOL,//23
    LEX_LBRACKET,
    LEX_NUM,//25
    LEX_MORE,
    LEX_LESS,//27
    LEX_NLESS,
    LEX_NMORE,//29
    LEX_EQUAL,
    LEX_NEQUAL,//31
    LEX_FIN,//32
    LEX_PERCENT,//33
    LEX_STR,//34
    LEX_ID,//35
    LEX_STAR,
    LEX_PLUS,
    LEX_MINUS,
    LEX_SLASH
};

class Lex
{
    type_of_lex t_lex;
    int v_lex;
public:
    Lex ( type_of_lex t = LEX_NULL, int v = 0)
    {
        t_lex = t;
        v_lex = v;
    }
    type_of_lex get_type () {
        return t_lex;
    }
    int get_value () {
        return v_lex;
    }
    friend ostream& operator<< ( ostream &s,const Lex& l )
    {
        s << '(' << l.t_lex << ',' << l.v_lex << ");";
        return s;
    }
    void set_value(int v)
    {
        v_lex=v;
    }
};

class ident
{
    char * name;
    bool declare;
    type_of_lex type;
    bool assign;
    int value;
public:
    ident ()
    {
        declare = false;
        assign = false;
    }
    ~ident()
    {
        delete[] name;
    }
    char *get_name ()
    {
        return name;
    }
    void put_name (const char *n)
    {
        name = new char [ strlen(n) + 1 ];
        strcpy ( name, n );
    }
    bool get_declare ()
    {
        return declare;
    }
    void put_declare ()
    {
        declare = true;
    }
    type_of_lex get_type ()
    {
        return type;
    }
    void put_type ( type_of_lex t )
    {
        type = t;
    }
    bool get_assign ()
    {
        return assign;
    }
    void put_assign ()
    {
        assign = true;
    }
    int get_value ()
    {
        return value;
    }
    void put_value (int v)
    {
        value = v;
    }
};
class tabl_ident
{
    ident * p;
    int size;
    int top;
public:
    tabl_ident ( int max_size )
    {
        p = new ident[size=max_size];
        top = 1;
    }
    ~tabl_ident ()
    {
        delete []p;
    }
    ident& operator[] ( int k )
    {
        return p[k];
    }
    int getCurr()
    {
        return top;
    }
    int put ( const char *buf );
};

int tabl_ident::put ( const char *buf )
{
    for ( int j=1; j<top; ++j )
        if ( !strcmp(buf, p[j].get_name()) )
            return j;
    p[top].put_name(buf);
    ++top;
    return top-1;
}

tabl_ident TID(100);
tabl_ident TSTR(100);

class Scanner
{
private:
    char * input;
    int iter;
    int len;
    char c;
    char buf[256];
    int buf_ind;
    static type_of_lex words[];
    static type_of_lex dlms[];
    state CS;
public:
    ~Scanner()
    {
        delete[] input;
    }
    int getPos()
    {
        return iter;
    }
    void setPos(int p)
    {
        iter=p;
        c=' ';
        clear();
    }
    void clear ()
    {
        buf_ind = 0;
        for (int j = 0; j < 256; ++j )
            buf[j] = '\0';
    }
    void add ()
    {
        buf[buf_ind++] = c;
    }
    void gc()
    {
        if (iter<len)
        {
            c=input[iter];
            iter++;
        } else throw "OUT OF CHARS";
    }
    Scanner(char* str,int size)
    {
        input = new char[size];
        strcpy(input,str);
        cout<<"INPUT: "<<input<<endl;
        len=size;
        iter=0;
        CS=H;
        //clear();
        gc();
    }
    int look(const char *buf,const char **list)
    {
        int i = 0;
        //cout<<"LOOKUP "<<buf<<"!"<<endl;
        while ( list[i]!=NULL )
        {
            if ( !strcmp(buf, list[i]) )
            {
                //cout<<"MATCH "<<list[i]<<" "<<i<<endl;
                return i;
            }
            ++i;
        }
        return 0;
    }
    Lex gl()
    {
        clear();
        CS=H;
        int d;
        do
        {
            switch(CS)
            {
            case H:
                if (c==' ' || c=='\n' || c=='\t' || c=='\r')
                    gc();
                else if (((c-'a')>=0 && (c-'z')<=0) || ((c-'A')>=0 && (c-'Z')<=0) || c=='*' || c=='_')
                {
                    clear();
                    add();
                    gc();
                    CS=IDENT;
                }
                else if(c-'0'>=0 && c-'9'<=0)
                {
                    d=c-'0';
                    gc();
                    CS=NUMB;
                }
                else if (c=='\0')
                    return Lex(LEX_FIN);
                else if(c=='\'')
                {
                    clear();
                    gc();
                    CS=STR;
                }
                else
                    CS=DELIM;
                break;
            case IDENT:
                if (((c-'a')>=0 && (c-'z')<=0) || ((c-'A')>=0 && (c-'Z')<=0) || ((c-'0'>=0 && c-'9'<=0)) || c=='_')
                {
                    add();
                    gc();
                }
                else
                {
                    if ( int j = look (buf, TW) )
                        return Lex (words[j], j);
                    else
                    {
                        j = TID.put(buf);
                        //clear();
                        return Lex (LEX_ID, j);
                    }
                }
                break;
            case STR:
            {
                if (c!='\'')
                {
                    add();
                    gc();
                }
                else
                {
                    gc();
                    int j= TSTR.put(buf);
                    return Lex(LEX_STR,j);
                }
            }
            break;
            case NUMB:
                if(c-'0'>=0 && c-'9'<=0)
                {
                    d=d*10+(c-'0');
                    gc();
                }
                else
                    return Lex(LEX_NUM, d);
                break;
            case DELIM:
                clear();
                add();
                gc ();
                if (c=='=')
                {
                    add();
                    gc();
                }
                if ( int j = look(buf, TD) )
                {
                    return Lex (dlms[j], j);
                }
                else
                    throw "LEXICAL ERROR";
                break;
            }
        } while(true);
    }
};

type_of_lex Scanner::words[] =
{
    LEX_NULL,
    LEX_SELECT,
    LEX_INSERT,
    LEX_UPDATE,
    LEX_DELETE,
    LEX_CREATE,
    LEX_BOOL,
    LEX_DROP,
    LEX_FROM,
    LEX_WHERE,
    LEX_INTO,
    LEX_IN,
    LEX_SET,
    LEX_TABLE,
    LEX_TEXT,
    LEX_LONG,
    LEX_LIKE,
    LEX_NOT,
    LEX_AND,
    LEX_OR,
    LEX_ALL,
    LEX_STAR,
    LEX_NULL
};
type_of_lex Scanner::dlms[] =
{
    LEX_NULL, // 0 ������� 0 �� ������������
    LEX_FIN, // 1
    LEX_COMMA, // 2
    LEX_LBRACKET, // 6
    LEX_RBRACKET, // 7
    LEX_LESS, // 9
    LEX_MORE, // 10
    LEX_EQUAL,
    LEX_NEQUAL, // 14
    LEX_NMORE, // 15
    LEX_NLESS,
    LEX_PLUS,
    LEX_MINUS,
    LEX_PERCENT,
    LEX_SLASH,
    LEX_NULL
};
//LEXICAL ANALYZER CODE ENDS HERE
//SYNTAX ANALYZER
//TODO:  BUGCHECKING AND SEMANTICS
template <class T, int max_size > class Stack
{
    T s[max_size];
    int top;
public:
    Stack() {
        top = 0;
    }
    Stack(Stack& st)
    {
        memcpy(s,st.s,sizeof(T)*max_size);
        top=st.top;
    }
    Stack& operator=(Stack& st)
    {
        memcpy(s,st.s,sizeof(T)*max_size);
        top=st.top;
        return *this;
    }
    void reset() {
        top = 0;
    }
    void push(T i);
    T pop();
    bool is_empty() {
        return top == 0;
    }
    bool is_full() {
        return top == max_size;
    }
};
template <class T, int max_size >
void Stack <T, max_size >::push(T i)
{
    if ( !is_full() )
    {
        s[top] = i;
        ++top;
    }
    else
        throw 1;
}
template <class T, int max_size >
T Stack <T, max_size >::pop()
{
    if ( !is_empty() )
    {
        --top;
        return s[top];
    }
    else
        throw 0;
}

class Parser
{
private:
    Scanner s;
    Lex lexem;
    THandle tH;
    unsigned int len;
    enum Errors e;
    type_of_lex tol;
    Stack<Lex,100> lexems;
    linSock* sock;
    int clientInd;
    int wherePos;
    char buf[256];//max string len
public:
    void clrBuf()
    {
        for (int j=0; j<256; j++)
            buf[j]='\0';
    }
    Parser(char* str, int size,linSock* lS,int ind):s(str,size)
    {
        wherePos=-1;
        sock=lS;
        clientInd=ind;
        getLex();
    }
    void getLex()
    {
        lexem=s.gl();
        tol=lexem.get_type();
        cout<<"LEXEM: "<<lexem<<endl;
    }
    void SQL()
    {
        switch(tol)
        {
        case LEX_SELECT:
            getLex();
            SELECT();
            cout<<"CLOSING TABLE"<<endl;
            e=closeTable(tH);
            if (e!=OK)
                throw getErrorString(e);
            break;
        case LEX_CREATE:
            getLex();
            CREATE();
            break;
        case LEX_INSERT:
            getLex();
            INSERT();
            e=closeTable(tH);
            if (e!=OK)
                throw getErrorString(e);
            break;
        case LEX_DROP:
            getLex();
            DROP();
            break;
        case LEX_DELETE:
            getLex();
            DELETE();
            e=closeTable(tH);
            if (e!=OK)
                throw getErrorString(e);
            break;
        case LEX_UPDATE:
            getLex();
            UPDATE();
            e=closeTable(tH);
            if (e!=OK)
                throw getErrorString(e);
            break;
        default:
            throw "SELECT OR DROP OR INSERT OR UPDATE OR DELETE OR CREATE EXPECTED";
        }
        getLex();
        if (tol!=LEX_FIN)
        {
            getLex();
            if (tol!=LEX_FIN)
                throw "END OF STRING EXPECTED";
        }
        cout<<"EVERYTHING SEEMS TO BE OK"<<endl;
    }
    void DELETE()
    {
        enum FieldType* fieldType =new enum FieldType[len];
        char ** fieldName=new char*[len];
        FROM();
        for (unsigned int i=len-1; i+1>0; i--)
        {
            fieldName[i]=NULL;
        }
        for (unsigned int i=len-1; i+1>0; i--) //couldnt do i>-1 cause unsigned
        {
            e=getFieldName(tH,i,&(fieldName[i]));
            if (e!=OK)
                throw getErrorString(e);
            e=getFieldType(tH,fieldName[i],&(fieldType[i]));
            if (e!=OK)
                throw getErrorString(e);
        }
        while (!afterLast(tH))
        {
            if (wherePos!=-1)
            {
                tol=LEX_WHERE;
                s.setPos(wherePos);
            }
            WHERE();
            int res = lexems.pop().get_value();
            cout<<"RESULT: "<<res<<endl;
            cout<<"getting field values"<<endl;
            if (res)
            {
                e=deleteRec(tH);
                if (e!=OK)
                    throw getErrorString(e);
            }
            e=moveNext(tH);
            if (e!=OK)
                throw getErrorString(e);
        }
    }
    void DROP()
    {
        if (tol!=LEX_TABLE)
            throw "TABLE EXPECTED";
        getLex();
        ID(1);
        char name[30];
        strcpy(name,TID[(lexems.pop()).get_value()].get_name());
        e=deleteTable(name);
        if(e!=OK)
            throw getErrorString(e);
    }
    void CREATE()
    {
        char name[30];
        cout<<"CREATE"<<endl;
        if (tol!=LEX_TABLE)
            throw "\'TABLE\' EXPECTED";
        getLex();
        ID(1);
        strcpy(name,TID[(lexems.pop()).get_value()].get_name()); //name of the table
        if(tol!=LEX_LBRACKET)
            throw "\'(\' EXPECTED";
        getLex();
        DESCRIPTION();
        getLex();
        while(tol==LEX_COMMA)
        {
            getLex();
            DESCRIPTION();
            getLex();
        }
        if(tol!=LEX_RBRACKET)
            throw "\')\' EXPECTED";
        //COMMAND CORRECT
        int k=0;
        struct TableStruct TS;
        TS.fieldsDef=new struct FieldDef[100];
        Lex l;
        try
        {
            while(true)
            {
                struct FieldDef FD;
                l=lexems.pop();
                if (l.get_type()==LEX_NUM)
                {
                    cout<<"STR"<<endl;
                    FD.len=l.get_value()*sizeof(char);
                    FD.type=Text;
                    try
                    {
                        l=lexems.pop();
                    }
                    catch(...)
                    {
                        throw "S STACK IS EMPTY";
                    }
                }
                else if (l.get_type()==LEX_ID)
                {
                    cout<<"NUM"<<endl;
                    FD.len=sizeof(long);
                    FD.type=Long;
                }
                else throw "S SIZE OF STRING EXPECTED";
                strcpy(FD.name,(TID[l.get_value()]).get_name());
                cout<<"TYPE "<<FD.type<<endl;
                TS.fieldsDef[k]=FD;
                k++;
            }
        }
        catch (int n)
        {
            cout<<"Who lives in a pineapple under the sea?"<<endl;
            cout<<"SpongeBob SquarePants!"<<endl;
            TS.numOfFields=k;
            e=createTable(name,&TS);
            if(e!=OK)
                throw getErrorString(e);
            return;
        }
        throw "S SOMETHING WENT WRONG";
    }
    void DESCRIPTION()
    {
        cout<<"DESCRIPTION"<<endl;
        ID(1);
        if (tol == LEX_TEXT)
        {
            getLex();
            if(tol!=LEX_LBRACKET)
                throw "\'(\' IN DESCRIPTION EXPECTED";
            getLex();
            if(tol!=LEX_NUM)
                throw "NUMBER EXPECTED";
            else lexems.push(lexem);
            getLex();
            if(tol!=LEX_RBRACKET)
                throw "\')\' IN DESCRIPTION EXPECTED";
        }
        else if(tol != LEX_LONG)
            throw "\'LONG\' OR \'TEXT\' EXPECTED";
    }
    void INSERT()
    {
        if (tol!=LEX_INTO)
            throw "\'INTO\' EXPECTED";
        getLex();
        ID(1);
        char name[30];
        strcpy(name,TID[(lexems.pop()).get_value()].get_name());
        e=openTable(name,&tH);
        if (e!=OK)
            throw getErrorString(e);
        if(tol!=LEX_LBRACKET)
            throw "\'(\' EXPECTED";
        getLex();
        ID(2);
        Lex l;
        char * fieldName;
        getFieldsNum(tH,&len);
        enum FieldType fieldType;
        e=createNew(tH);
        if (e!=OK)
            throw getErrorString(e);
        // for (unsigned int i=0;i<len;i++)
        // {
        // e=getFieldName(tH,i,&fieldName);
        // if (e!=OK)
        // throw getErrorString(e);
        // cout<<"field Name: "<<fieldName<<endl;
        // e=getFieldType(tH,fieldName,&fieldType);
        // if (e!=OK)
        // throw getErrorString(e);
        // cout<<fieldType<<" "<<Long<<Text<<endl;
        // }
        try
        {
            for(unsigned int i=0; i<len; i++)
            {
                // cout<<"GOING"<<endl;
                l=lexems.pop();
                e=getFieldName(tH,i,&fieldName);
                if (e!=OK)
                    throw getErrorString(e);
                //cout<<"field Name: "<<fieldName<<endl;
                e=getFieldType(tH,fieldName,&fieldType);
                if (e!=OK)
                    throw getErrorString(e);
                //cout<<fieldType<<" "<<Long<<Text<<endl;
                if (l.get_type()==LEX_NUM) //not checking fieldtypes cause it is already implemented
                {
                    e=putLongNew(tH,fieldName,l.get_value());
                    if (e!=OK)
                        throw getErrorString(e);
                }
                else if (l.get_type()==LEX_STR)
                {
                    cout<<"STR "<<fieldName<<endl;
                    cout<<(TSTR[l.get_value()]).get_name()<<endl;
                    e=putTextNew(tH,fieldName,(TSTR[l.get_value()]).get_name());
                    if (e!=OK)
                        throw getErrorString(e);
                }
                //else throw "S STR OR NUM EXPECTED"; cause already checked in ID
            }
        }
        catch (int n)
        {
            throw "TOO FEW FIELDS";
        }
        if (!lexems.is_empty())
            throw "TOO MANY FIELDS";
        cout<<"Who lives in a pineapple under the sea?"<<endl;
        cout<<"SpongeBob SquarePants!"<<endl;
        e=insertzNew(tH);
        if (e!=OK)
            throw getErrorString(e);
        if (tol!=LEX_RBRACKET)
            throw "\')\' EXPECTED";
        if (!lexems.is_empty())
            throw "S SOMETHING WENT WRONG";
    }
    void UPDATE()
    {
        type_of_lex t;
        ID(1);
        e=openTable(TID[lexems.pop().get_value()].get_name(),&tH);
        if (e!=OK)
            throw getErrorString(e);
        e=getFieldsNum(tH,&len);
        if (e!=OK)
            throw getErrorString(e);
        if (tol!=LEX_SET)
            throw "\'SET\' EXPECTED";
        getLex();
        ID(1);
        enum FieldType fieldType;
        char* name =TID[lexems.pop().get_value()].get_name();
        cout<<"NAME: "<<name<<endl;
        e=getFieldType(tH,name,&fieldType);
        if (e!=OK)
            throw getErrorString(e);
        if (tol!=LEX_EQUAL)
            throw "\'=\' EXPECTED";
        if (fieldType==Text)
            t=LEX_STR;
        else
            t=LEX_NUM;
        cout<<"E1"<<endl;
        int Pos=-1;
        do
        {
            if (Pos==-1)
                Pos=s.getPos();
            else
                s.setPos(Pos);
            getLex();
            E1();
            Lex l=lexems.pop();
            cout<<"RESULT: "<<l<<endl;
            if (l.get_type()!=t)
                throw "WRONG TYPES";
            WHERE();
            int res = lexems.pop().get_value();
            cout<<"RESULT: "<<res<<endl;
            cout<<"getting field values"<<endl;
            if (res)
            {
                e=startEdit(tH);
                if (e!=OK)
                    throw getErrorString(e);
                if (fieldType==Text)
                {
                    char * str = TSTR[l.get_value()].get_name();
                    e=putText(tH,name,str);
                    if (e!=OK)
                        throw getErrorString(e);
                }
                else
                {
                    long val=l.get_value();
                    e=putLong(tH,name,val);
                    if (e!=OK)
                        throw getErrorString(e);
                }
                e=finishEdit(tH);
                if (e!=OK)
                    throw getErrorString(e);
            }
            e=moveNext(tH);
            if (e!=OK)
                throw getErrorString(e);
        } while (!afterLast(tH));
    }
    void SELECT()
    {   // * STAR SPECIAL
        cout<<"SELECT"<<endl;
        enum FieldType* fieldType;
        char ** fieldName;
        if (tol==LEX_ID)
        {
            ID(0);
            FROM();//launching ID(1) so no need in getting another lexem
            fieldType=new enum FieldType[len];
            fieldName=new char*[len];
            for (unsigned int i=len-1; i+1>0; i--)
            {
                fieldName[i]=NULL;
            }
            Lex l;
            unsigned int k=0;
            try
            {
                while((l=lexems.pop()).get_type()==LEX_ID)
                {
                    //cout<<l<<endl;
                    //if (k>len)
                    // throw "WRONG FIELDS IN SELECT";
                    k=(unsigned int)-1;
                    for(unsigned int i=len-1; i+1>0; i--)
                    {
                        char* name;
                        e=getFieldName(tH,i,&name);
                        if (!strcmp(name,TID[l.get_value()].get_name()))
                            k=i;
                    }
                    if (k==(unsigned int)-1)
                        throw "WRONG FIELD/S SPECIFIED";
                    fieldName[k]=new char[strlen(TID[l.get_value()].get_name())+1];
                    strcpy(fieldName[k],TID[l.get_value()].get_name());
                    e=getFieldType(tH,fieldName[k],&(fieldType[k]));
                    if (e!=OK)
                        throw getErrorString(e);
                    k++;
                }
                lexems.push(l);
            }
            catch (int n)
            {
            }
            //TODO: EVERYTHING ELSE
            //WHERE();
        }
        else if (tol==LEX_STAR)
        {
            cout<<"STAR"<<endl;
            getLex();
            FROM();
            fieldType=new enum FieldType[len];
            fieldName=new char*[len];
            for (unsigned int i=len-1; i+1>0; i--)
            {
                fieldName[i]=NULL;
            }
            for (unsigned int i=len-1; i+1>0; i--) //couldnt do i>-1 cause unsigned
            {
                e=getFieldName(tH,i,&(fieldName[i]));
                if (e!=OK)
                    throw getErrorString(e);
                e=getFieldType(tH,fieldName[i],&(fieldType[i]));
                if (e!=OK)
                    throw getErrorString(e);
            }
            //WHERE();
        }
        else
            throw "WRONG FIRST ARGUMENT OF SELECT";
        //GETTING VALUES
        e=moveFirst(tH);
        if (e!=OK)
            throw getErrorString(e);
        cout<<"WHILE?"<<endl;
        while (!afterLast(tH))
        {
            cout<<"CYCLE"<<endl;
            if (wherePos!=-1)
            {
                tol=LEX_WHERE;
                s.setPos(wherePos);
            }
            WHERE();
            int res = lexems.pop().get_value();
            cout<<"RESULT: "<<res<<endl;
            cout<<"getting field values"<<endl;
            if (res)
            {
                for (unsigned int i=len-1; i+1>0; i--) //couldnt do i>-1 cause unsigned
                {
                    clrBuf();
                    if (fieldName[i]==NULL)
                        continue;
                    if (fieldType[i]==Long)
                    {
                        long p=0;
                        e=getLong(tH,fieldName[i],&p);
                        if (e!=OK)
                            throw getErrorString(e);
                        cout<<"LONG: "<<p<<endl;
                        sprintf(buf,"%ld",p);
                    }
                    else
                    {
                        char *b;
                        e=getText(tH,fieldName[i],&b);
                        if (e!=OK)
                            throw getErrorString(e);
                        cout<<"TEXT: "<<b<<endl;
                        strcat(buf,b);
                    }
                    //cout<<buf<<endl;
                    strcat(buf," ");
                    (*sock).send(buf,strlen(buf)+1,clientInd);
                }
                (*sock).send("\n",2,clientInd);
            }
            e=moveNext(tH);
            if (e!=OK)
                throw getErrorString(e);
        }
        cout<<"END OF SELECT"<<endl;
    }
    void FROM()
    {
        cout<<"FROM"<<endl;
        if (tol!=LEX_FROM)
            throw "FROM EXPECTED";
        else
        {
            getLex();
            ID(1);
            e=openTable(TID[lexems.pop().get_value()].get_name(),&tH);
            if (e!=OK)
                throw getErrorString(e);
            e=getFieldsNum(tH,&len);
            if (e!=OK)
                throw getErrorString(e);
        }
    }
    void ID(int mode)//0-only ids many of them, 1- only ids only one 2-NUMS STRS many of them 3-NUM OR STR only one
    {   //always got unprocessed lexem at the end
        cout<<"ID"<<endl;
        if ((tol!=LEX_ID || mode>=2) && ((tol!=LEX_STR && tol!=LEX_NUM) || mode<2))
            throw "NONRESERVED WORD EXPECTED";
        lexems.push(lexem);
        getLex();
        //cout<<"CURR1: "<<lexem<<endl;
        if (mode==0 || mode==2)
            while(tol==LEX_COMMA)
            {
                //cout<<"CURR2: "<<lexem<<endl;
                getLex();
                if ((tol!=LEX_ID || mode>=2) && ((tol!=LEX_STR && tol!=LEX_NUM) || mode<2))
                    throw "NONRESERVED WORD EXPECTED";
                else
                {
                    lexems.push(lexem);
                    getLex();
                }
            }
        //cout<<"CURR3: "<<lexem<<endl;
    }
    void WHERE()
    {   //BRACKETS
        if (tol!=LEX_WHERE)
        {
            throw "\'WHERE\' EXPECTED";
        }
        cout<<"WHERE"<<endl;
        if (wherePos==-1)
            wherePos=s.getPos();
        getLex();
        switch(tol)
        {
        case LEX_ALL:
            lexems.push(Lex(LEX_BOOL,1));
            return;
            break;
        case LEX_NOT:
        case LEX_ID:
        case LEX_STR:
        case LEX_NUM:
        case LEX_LBRACKET:
        {
            EXPRESSION();
            //getLex();
            bool b=false;
            if (tol==LEX_NOT)
            {
                getLex();
                b=true;
            }
            if (tol==LEX_LIKE)
            {
                cout<<"LIKE"<<endl;
                Lex l=lexems.pop();
                if (l.get_type()!=LEX_ID)
                    throw "TEXT FIELD EXPECTED";
                enum FieldType fieldType;
                e=getFieldType(tH,TID[l.get_value()].get_name(),&fieldType);
                if (e!=OK)
                    throw getErrorString(e);
                char* val;
                if (fieldType==Text)
                {
                    e=getText(tH,TID[l.get_value()].get_name(),&val);
                    if (e!=OK)
                        throw getErrorString(e);
                    l=Lex(LEX_STR,TSTR.put(val));
                }
                else
                    throw "TEXT FIELD EXPECTED";
                getLex();
                if (tol!=LEX_STR)
                    throw "STRING EXPECTED AFTER LIKE";
                char* likestr=TSTR[lexem.get_value()].get_name();
                lexems.push(Lex(LEX_BOOL,LIKE(val,likestr)));;
            }
            else if (tol==LEX_IN)
            {
                WHEREIN(b);
            }
            Lex res = lexems.pop();
            if (res.get_type()!=LEX_BOOL)
                throw "WRONG WHERE EXPRESSION";
            lexems.push(res);
        }   //else throw "\'IN\' EXPECTED IN WHERE SENTENCE";
        break;
        default:
            throw "EXPRESSION OR \'ALL\' EXPECTED";
        }
    }
    bool LIKE(char* val,char* likestr)
    {
        switch (*likestr)
        {
        case '%':
            for(int i=0; i<=(int)strlen(val); i++)
            {
                if (LIKE(&(val[i]),&(likestr[1])))
                    return true;
            }
            return false;
            break;
        case '[':
        {
            int offset=0;
            bool symbols[256];
            getSymbols(likestr,&offset,symbols);
            return (symbols[(int)(*val)]) && LIKE(&(val[1]),&(likestr[offset+1]));
        }
        break;
        case '_':
            return LIKE(&(val[1]),&(likestr[1]));
        case '\0':
            if (*val==*likestr)
                return true;
            else return false;
        default:
            if (*val==*likestr)
                return LIKE(&(val[1]),&(likestr[1]));
            else
                return false;
        }
        return true;
    }
    void getSymbols(char* likestr,int* offset,bool symbols[256])
    {
        //bool symbols[256];
        bool initValue=false;
        int k=1;
        if (likestr[1]=='^')
        {
            initValue=true;
            k=2;
        }
        for(int i=0; i<256; i++)
            symbols[i]=initValue;
        if (likestr[k+1]=='-')
        {
            for(int i=(int)(likestr[k]); i<=(int)(likestr[k+2]); i++)
                symbols[i]=!symbols[i];
            if (likestr[k+3]!=']')
                throw "WRONG LIKE STRING";
            k=k+3;
        }
        else
        {
            while(likestr[k]!=']' && likestr[k]!='\0')
            {
                symbols[(int)(likestr[k])]=!symbols[(int)(likestr[k])];
                k++;
            }
        }
        *offset=k;
        //return symbols;
    }
    void WHEREIN(bool n)
    {
        Lex result=lexems.pop();
        if (result.get_type() == LEX_ID)
        {
            enum FieldType fieldType;
            e=getFieldType(tH,TID[result.get_value()].get_name(),&fieldType);
            if (e!=OK)
                throw getErrorString(e);
            if (fieldType==Text)
            {
                char* val;
                e=getText(tH,TID[result.get_value()].get_name(),&val);
                if (e!=OK)
                    throw getErrorString(e);
                result = Lex(LEX_STR,TSTR.put(val));
            }
            else
            {
                long val;
                e=getLong(tH,TID[result.get_value()].get_name(),&val);
                if (e!=OK)
                    throw getErrorString(e);
                result=Lex(LEX_NUM,val);
            }
        }
        else if (result.get_type()!=LEX_NUM && result.get_type()!=LEX_STR)
            throw "LONG OR TEXT EXPRESSION EXPECTED";
        cout<<"IN "<<result<<endl;
        getLex();
        if (tol==LEX_LBRACKET)
        {
            getLex();
            ID(2);
            Lex l;
            bool isIn=false;
            while(!lexems.is_empty())
            {
                l=lexems.pop();
                cout<<"WORKING: "<<l<<endl;
                if ((l.get_type() ==result.get_type()) && (l.get_value() == result.get_value()))//just values cause strings in TSTR do not double
                {
                    cout<<"FOUND: "<<l<<endl;
                    isIn=true;
                    break;
                }
            }
            if (n)
                isIn=!isIn;
            lexems.push(Lex(LEX_BOOL,(int)isIn));
            if (tol!=LEX_RBRACKET)
                throw "\')\' EXPECTED AFTER \'IN\'";;
        }
        else throw "\'(\' EXPECTED AFTER \'IN\'";
    }
    void EXPRESSION()
    {
        cout<<"E"<<endl;
        E1();
        if (tol==LEX_EQUAL || tol == LEX_NEQUAL || tol==LEX_LESS || tol==LEX_MORE || tol==LEX_NLESS || tol==LEX_NMORE)
        {
            lexems.push(lexem);
            getLex();
            E1();
            check_op();
        }
    }
    void E1()
    {
        cout<<"E1"<<endl;
        T();
        while(tol==LEX_PLUS || tol==LEX_MINUS || tol==LEX_OR)
        {
            lexems.push(lexem);
            getLex();
            T();
            check_op();
        }
    }
    void T ()
    {
        cout<<"T"<<endl;
        F();
        while ( tol==LEX_STAR || tol==LEX_SLASH || tol==LEX_AND || tol==LEX_PERCENT)
        {
            lexems.push(lexem);
            getLex();
            F();
            check_op();
        }
    }
    void F()
    {
        cout<<"F "<<lexem<<endl;
        switch(tol)
        {
        case LEX_STR:
            lexems.push(lexem);
            getLex();
            break;
        case LEX_ID:
            lexems.push(lexem);
            getLex();
            break;
        case LEX_NUM:
            lexems.push(lexem);
            getLex();
            break;
        case LEX_NOT:
            getLex();
            F();
            check_not();
            break;
        case LEX_LBRACKET:
            getLex();
            EXPRESSION();
            if (tol != LEX_RBRACKET)
                throw "\')\' EXPECTED";
            else getLex();
            break;
        default:
            throw "WRONG EXPRESSION";
        }
    }
    void check_op ()//seems to be working fine
    {
        type_of_lex t1, t2, op, t = LEX_NUM, r = LEX_BOOL;
        Lex lexem2=lexems.pop();
        Lex lexemop=lexems.pop();
        Lex lexem1=lexems.pop();
        t2 = lexem2.get_type();
        op = lexemop.get_type();
        t1 = lexem1.get_type();
        if ( op==LEX_PLUS || op==LEX_MINUS || op==LEX_STAR || op==LEX_SLASH || op == LEX_PERCENT )
            r = LEX_NUM;
        if ( op == LEX_OR || op == LEX_AND )
            t = LEX_BOOL;
        if (t1==LEX_ID)
        {
            enum FieldType fieldType;
            e=getFieldType(tH,TID[lexem1.get_value()].get_name(),&fieldType);
            if (e!=OK)
                throw getErrorString(e);
            if (fieldType==Text)
            {
                t1=LEX_STR;
                char* val;
                e=getText(tH,TID[lexem1.get_value()].get_name(),&val);
                if (e!=OK)
                    throw getErrorString(e);
                lexem1.set_value(TSTR.put(val));
            }
            else
            {
                t1=LEX_NUM;
                long val;
                e=getLong(tH,TID[lexem1.get_value()].get_name(),&val);
                if (e!=OK)
                    throw getErrorString(e);
                lexem1.set_value(val);
            }
        }
        if (t2==LEX_ID)
        {
            enum FieldType fieldType;
            e=getFieldType(tH,TID[lexem2.get_value()].get_name(),&fieldType);
            if (e!=OK)
                throw getErrorString(e);
            if (fieldType==Text)
            {
                t2=LEX_STR;
                char* val;
                e=getText(tH,TID[lexem2.get_value()].get_name(),&val);
                if (e!=OK)
                    throw getErrorString(e);
                lexem2.set_value(TSTR.put(val));
            }
            else
            {
                t2=LEX_NUM;
                long val;
                e=getLong(tH,TID[lexem2.get_value()].get_name(),&val);
                if (e!=OK)
                    throw getErrorString(e);
                lexem2.set_value(val);
            }
        }
        cout<<t1<<" "<<t2<<" "<<r<<" "<<t<<endl;
        if ( t1 == t2 && (t1 == t || t==LEX_NUM))
        {
            int value=0;
            cout<<"OP: "<<op<<endl;
            switch(op)
            {
            case LEX_PLUS:
                value = lexem1.get_value()+lexem2.get_value();
                break;
            case LEX_MINUS:
                value = lexem1.get_value()-lexem2.get_value();
                break;
            case LEX_STAR:
                value = lexem1.get_value()*lexem2.get_value();
                break;
            case LEX_SLASH:
                value = lexem1.get_value()/lexem2.get_value();
                break;
            case LEX_PERCENT:
                value = lexem1.get_value()%lexem2.get_value();
                break;
            case LEX_OR:
                value = lexem1.get_value()||lexem2.get_value();
                break;
            case LEX_AND:
                value = lexem1.get_value()&&lexem2.get_value();
                break;
            case LEX_EQUAL:
                if(t1==LEX_NUM)
                    value = lexem1.get_value()==lexem2.get_value();
                else
                    value=!strcmp(TSTR[lexem1.get_value()].get_name(),TSTR[lexem2.get_value()].get_name());
                break;
            case LEX_NEQUAL:
                if(t1==LEX_NUM)
                    value = lexem1.get_value()!=lexem2.get_value();
                else
                    value=strcmp(TSTR[lexem1.get_value()].get_name(),TSTR[lexem2.get_value()].get_name());
                break;
            case LEX_MORE:
                if(t1==LEX_NUM)
                    value = lexem1.get_value()>lexem2.get_value();
                else
                    value=(strcmp(TSTR[lexem1.get_value()].get_name(),TSTR[lexem2.get_value()].get_name())>0);
                break;
            case LEX_NMORE:
                if(t1==LEX_NUM)
                    value = lexem1.get_value()<=lexem2.get_value();
                else
                    value=(strcmp(TSTR[lexem1.get_value()].get_name(),TSTR[lexem2.get_value()].get_name())<=0);
                break;
            case LEX_LESS:
                if (t1==LEX_NUM)
                    value = lexem1.get_value()<lexem2.get_value();
                else
                    value=(strcmp(TSTR[lexem1.get_value()].get_name(),TSTR[lexem2.get_value()].get_name())<0);
                break;
            case LEX_NLESS:
                if (t1==LEX_NUM)
                    value = lexem1.get_value()>=lexem2.get_value();
                else
                    value=(strcmp(TSTR[lexem1.get_value()].get_name(),TSTR[lexem2.get_value()].get_name())>=0);
                break;
            default:
                throw "SOMETHING WENT WRONG IN OPERATION CHECK";
            }
            cout<<"TYPE: "<<r<<"VALUE: "<<value<<" V1: "<<lexem1.get_value()<<" V2: "<<lexem2.get_value()<<endl;
            lexems.push(Lex(r,value));
        }
        else
            throw "wrong types are in operation";
    }
    void check_not ()
    {
        Lex l=lexems.pop();
        if (l.get_type() != LEX_BOOL)
            throw "wrong type is in not";
        else
        {
            lexems.push (Lex(LEX_BOOL,!l.get_value()));
        }
    }
};

